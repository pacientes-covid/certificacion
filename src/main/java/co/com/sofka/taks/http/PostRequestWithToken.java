package co.com.sofka.taks.http;

import co.com.sofka.helper.Constants;
import io.restassured.http.ContentType;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class PostRequestWithToken implements Task {

    private final Object bodyRequest;

    public PostRequestWithToken(Object bodyRequest) {
        this.bodyRequest = bodyRequest;

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to("").with(
                request -> request.contentType(ContentType.JSON)
                        .auth().oauth2(Serenity.sessionVariableCalled(Constants.VAR_TOKEN))
                        .body(bodyRequest)));

    }

    public static PostRequestWithToken executed(Object bodyRequest) {
        return Tasks.instrumented(PostRequestWithToken.class, bodyRequest);
    }

}
