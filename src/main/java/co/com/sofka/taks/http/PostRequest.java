package co.com.sofka.taks.http;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class PostRequest implements Task {
    private final Object bodyRequest;

    public PostRequest(Object bodyRequest) {
        this.bodyRequest = bodyRequest;

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to("").with(request -> request.contentType(ContentType.JSON)
                .body(bodyRequest)));

    }

    public static PostRequest executed(Object bodyRequest) {
        return Tasks.instrumented(PostRequest.class, bodyRequest);
    }

}
