package co.com.sofka.taks.http;

import co.com.sofka.helper.Constants;
import io.restassured.http.ContentType;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class GetRequestWithToken implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Get.resource("").with(
                request -> request.contentType(ContentType.JSON)
                        .auth().oauth2(Serenity.sessionVariableCalled(Constants.VAR_TOKEN))));

    }

    public static GetRequestWithToken executed() {
        return Tasks.instrumented(GetRequestWithToken.class);
    }

}