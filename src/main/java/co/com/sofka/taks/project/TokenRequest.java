package co.com.sofka.taks.project;

import co.com.sofka.data.request.authenticate.RequestAuthenticate;
import co.com.sofka.helper.Constants;
import co.com.sofka.taks.http.PostRequest;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

public class TokenRequest implements Task {

    EnvironmentVariables variables = null;


    @Override
    public <T extends Actor> void performAs(T actor) {
        String endpoint;
        endpoint = variables.optionalProperty("apipacientescovid.authenticate")
                .orElse("http://localhost:8090/paciente-covid/api/auth/authenticate");
        actor.whoCan(CallAnApi.at(endpoint));
        actor.attemptsTo(PostRequest.executed(RequestAuthenticate.getNewValidUser()));
        Serenity.setSessionVariable(Constants.VAR_TOKEN).to(SerenityRest.lastResponse()
                .jsonPath()
                .getObject("jwt", String.class));
    }

    public static TokenRequest executed() {
        return Tasks.instrumented(TokenRequest.class);
    }

}
