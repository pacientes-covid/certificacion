package co.com.sofka.taks.project;

import co.com.sofka.data.request.patient.RequestPatient;
import co.com.sofka.helper.Constants;
import co.com.sofka.model.patient.JsonPatient;
import co.com.sofka.taks.http.PostRequestWithToken;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class CreatedPatientRequest implements Task {

    EnvironmentVariables variables = null;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(TokenRequest.executed());
        String endpoint;
        endpoint = variables.optionalProperty("apipacientescovid.savepatient")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/save");
        actor.whoCan(CallAnApi.at(endpoint));
        JsonPatient patient = RequestPatient.getNewPatient();
        actor.attemptsTo(PostRequestWithToken.executed(patient));
        Serenity.setSessionVariable(Constants.VAR_PATIENT_ID).to(String.valueOf(patient.getPatientId()));
        actor.should(seeThatResponse("El paciente fue creado correctamente",
                respose -> respose.statusCode(201)));
    }

    public static CreatedPatientRequest executed() {
        return Tasks.instrumented(CreatedPatientRequest.class);
    }

}
