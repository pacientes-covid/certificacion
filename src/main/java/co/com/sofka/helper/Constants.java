package co.com.sofka.helper;

public class Constants {

    private Constants(){
    }

    public static final String USUARIO_API = "Covid";
    public static final String CLAVE_API = "Covid";
    public static final String CLAVE_INVALIDA_API = "Error";
    public static final String TOKEN_INVALIDO = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJDb3ZpZCIsImlhdCI6MTYxNTM4NzczMSwiZXhwIjoxNjE1NDIzNzMxfQ.LfY0zxaorMDESfxCbQIapIIRZX-3HLQgtyekIoDArfa";


    public static final String VAR_TOKEN = "TOKEN";
    public static final String VAR_PATIENT_ID = "PATIENT_ID";

}
