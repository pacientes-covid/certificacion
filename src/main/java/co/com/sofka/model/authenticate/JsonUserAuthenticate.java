package co.com.sofka.model.authenticate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JsonUserAuthenticate {

    private String username;
    private String password;
}
