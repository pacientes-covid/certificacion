package co.com.sofka.model.patient;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JsonPatient {

    private String address;
    private long cellPhoneNumber;
    private String email;
    private String firstName;
    private String lastName;
    private long patientId;
}
