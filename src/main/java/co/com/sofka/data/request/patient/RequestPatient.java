package co.com.sofka.data.request.patient;

import co.com.sofka.model.patient.JsonPatient;
import com.github.javafaker.Faker;

import java.security.SecureRandom;
import java.util.Locale;

public class RequestPatient {

    private  RequestPatient(){
    }

    private static Faker faker = Faker.instance(new Locale("es", "CO"), new SecureRandom());

    public static JsonPatient getNewPatient() {
        return JsonPatient.builder()
                .address(faker.address().fullAddress())
                .cellPhoneNumber(Long.parseLong(faker.number().digits(9)))
                .email(faker.internet().emailAddress())
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .patientId(Long.parseLong(faker.number().digits(9)))
                .build();
    }

    public static JsonPatient getIncompletedPatient() {
        return JsonPatient.builder()
                .address(faker.address().fullAddress())
                .cellPhoneNumber(Long.parseLong(faker.number().digits(9)))
                .email(faker.internet().emailAddress())
                .lastName(faker.name().lastName())
                .patientId(Long.parseLong(faker.number().digits(9)))
                .build();
    }

    public static String getNewPatientId() {
        return faker.number().digits(9);
    }
}
