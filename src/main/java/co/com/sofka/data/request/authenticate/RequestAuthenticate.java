package co.com.sofka.data.request.authenticate;

import co.com.sofka.helper.Constants;
import co.com.sofka.model.authenticate.JsonUserAuthenticate;

public class RequestAuthenticate {

    private RequestAuthenticate(){
    }

    public static JsonUserAuthenticate getNewValidUser(){
        return JsonUserAuthenticate.builder()
                .username(Constants.USUARIO_API)
                .password(Constants.CLAVE_API)
                .build();
    }

    public static JsonUserAuthenticate getNewInValidUser(){
        return JsonUserAuthenticate.builder()
                . username(Constants.USUARIO_API)
                .password(Constants.CLAVE_INVALIDA_API)
                .build();
    }
}
