#language: es

Característica: Administrar información de pacientes

  Escenario: Creacion de paciente exitosa
    Dado un "auxiliar de enfermeria" autenticado correctamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de creacion de pacientes
    Cuando se tiene la informacion de un paciente
    Entonces se puede crear el paciente en la api

  Escenario: Creacion de paciente fallida token invalido
    Dado un "auxiliar de enfermeria" autenticado incorrectamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de creacion de pacientes
    Cuando se tiene la informacion de un paciente
    Entonces la api de pacientes contesta con codigo de error "403"

  Escenario: Creacion de paciente fallida al no enviar campos obligatorios
    Dado un "auxiliar de enfermeria" autenticado correctamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de creacion de pacientes
    Cuando se tiene la informacion incompleta de un paciente
    Entonces la api de pacientes contesta con codigo de error "500"


  Escenario: Consulta exitosa de paciente por número de documento
    Dado que un "auxiliar de enfermeria" guardo previamente un paciente
    Y el auxiliar de enfermeria conoce la ruta de la api de consulta de paciente
    Cuando se consulta la informacion del paciente
    Entonces la api entrega la informacion del paciente

  Escenario: Consulta fallida de paciente token invalido
    Dado que un "auxiliar de enfermeria" guardo previamente un paciente
    Y un "auxiliar de enfermeria" autenticado incorrectamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de consulta de paciente
    Cuando se consulta la informacion del paciente
    Entonces la api de pacientes contesta con codigo de error "403"


  Escenario: Consulta fallida paciente no existente
    Dado que un "auxiliar de enfermeria" tiene un numero de documento de paciente no existente
    Y el auxiliar de enfermeria conoce la ruta de la api de consulta de paciente
    Cuando se consulta la informacion del paciente
    Entonces la api de pacientes contesta con codigo de error "404"


  Escenario: Consulta exitosa de todos los pacientes
    Dado un "auxiliar de enfermeria" autenticado correctamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de consulta de todos los pacientes
    Cuando se consulta la informacion de todos los paciente
    Entonces la api entrega una lista con la informacion de todos los pacientes


  Escenario: Consulta fallida de todos los pacientes token invalido
    Dado un "auxiliar de enfermeria" autenticado incorrectamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de consulta de todos los pacientes
    Cuando se consulta la informacion de todos los paciente
    Entonces la api de pacientes contesta con codigo de error "403"


  Escenario: Eliminación exitosa de paciente por número de documento
    Dado que un "auxiliar de enfermeria" guardo previamente un paciente
    Y el auxiliar de enfermeria conoce la ruta de la api de eliminacion de paciente
    Cuando se elimina la informacion del paciente
    Entonces la api entrega el resultado exitoso en la eliminacion

  Escenario: Eliminación fallida de paciente token invalido
    Dado que un "auxiliar de enfermeria" guardo previamente un paciente
    Y un "auxiliar de enfermeria" autenticado incorrectamente en la api
    Y el auxiliar de enfermeria conoce la ruta de la api de eliminacion de paciente
    Cuando se elimina la informacion del paciente
    Entonces la api de pacientes contesta con codigo de error "403"


  Escenario: Eliminación fallida de paciente no existente
    Dado que un "auxiliar de enfermeria" tiene un numero de documento de paciente no existente
    Y el auxiliar de enfermeria conoce la ruta de la api de eliminacion de paciente
    Cuando se elimina la informacion del paciente
    Entonces la api de pacientes contesta con codigo de error "404"