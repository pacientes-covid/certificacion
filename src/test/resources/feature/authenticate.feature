#language: es

  Característica: Autenticación de usuario en la api
    Antecedentes:
      Dado un "usuario" que conoce la ruta de la api de autenticacion

    Escenario: Autenticacion exitosa
      Cuando el usuario tiene credenciales validas
      Entonces se puede autenticar en la api correctamente

    Escenario: Autenticacion fallida
      Cuando el usuario tiene credenciales incorrectas
      Entonces la api contesta con codigo de error "403"
