package screenplay.steps;

import co.com.sofka.data.request.authenticate.RequestAuthenticate;
import co.com.sofka.taks.http.PostRequest;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.startsWith;


public class Authenticate {
    EnvironmentVariables variables;
    String endpoint;
    private Actor actor;

    @Dado("^un \"([^\"]*)\" que conoce la ruta de la api de autenticacion$")
    public void unQueConoceLaRutaDeLaApiDeAutenticacion(String name) {
        actor = Actor.named(name);
        endpoint = variables.optionalProperty("apipacientescovid.authenticate")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/save");
        actor.whoCan(CallAnApi.at(endpoint));
    }

    @Cuando("^el usuario tiene credenciales validas$")
    public void elUsuarioTieneCredencialesValidas() {
        actor.attemptsTo(PostRequest.executed(RequestAuthenticate.getNewValidUser()));
    }


    @Entonces("^se puede autenticar en la api correctamente$")
    public void sePuedeAutenticarEnLaApiCorrectamente() {
        actor.should(seeThatResponse("El token fue creado correctamente",
                respose -> respose.statusCode(200).body(startsWith("{\"jwt\":\"ey"))));
    }

    @Cuando("^el usuario tiene credenciales incorrectas$")
    public void elUsuarioTieneCredencialesIncorrectas() {
        actor.attemptsTo(PostRequest.executed(RequestAuthenticate.getNewInValidUser()));
    }


    @Entonces("^la api contesta con codigo de error \"([^\"]*)\"$")
    public void laApiContestaConCodigoDeError(String codigo) {
        actor.should(seeThatResponse("Codigo de error " + codigo + " entregado correctamente",
                respose -> respose.statusCode(Integer.parseInt(codigo))));
    }
}
