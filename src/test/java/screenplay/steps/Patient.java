package screenplay.steps;

import co.com.sofka.data.request.patient.RequestPatient;
import co.com.sofka.helper.Constants;
import co.com.sofka.model.patient.JsonPatient;
import co.com.sofka.taks.http.DeleteRequestWithToken;
import co.com.sofka.taks.http.GetRequestWithToken;
import co.com.sofka.taks.http.PostRequestWithToken;
import co.com.sofka.taks.project.CreatedPatientRequest;
import co.com.sofka.taks.project.TokenRequest;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.Arrays;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class Patient {

    EnvironmentVariables variables;
    String endpoint;
    JsonPatient patientSend, patientRecieved;
    JsonPatient[] patients;
    private Actor actor;

    @Dado("^un \"([^\"]*)\" autenticado correctamente en la api$")
    public void unAutenticadoCorrectamenteEnLaApi(String name) {
        actor = Actor.named(name);
        actor.attemptsTo(TokenRequest.executed());
    }

    @Y("^el auxiliar de enfermeria conoce la ruta de la api de creacion de pacientes$")
    public void elAuxiliarDeEnfermeriaConoceLaRutaDeLaApiDeCreacionDePacientes() {
        endpoint = variables.optionalProperty("apipacientescovid.savepatient")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/save");
        actor.whoCan(CallAnApi.at(endpoint));
    }

    @Cuando("^se tiene la informacion de un paciente$")
    public void seTieneLaInformacionDeUnPaciente() {
        patientSend = RequestPatient.getNewPatient();
        actor.attemptsTo(PostRequestWithToken.executed(patientSend));
    }

    @Entonces("^se puede crear el paciente en la api$")
    public void sePuedeCrearElPacienteEnLaApi() {
        actor.should(seeThatResponse("El paciente fue creado correctamente",
                respose -> respose.statusCode(201)));
        patientRecieved = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonPatient.class);
        assertThat(patientRecieved).isEqualToComparingOnlyGivenFields(patientSend);
    }

    @Dado("^un \"([^\"]*)\" autenticado incorrectamente en la api$")
    public void unAutenticadoIncorrectamenteEnLaApi(String name) {
        actor = Actor.named(name);
        Serenity.setSessionVariable(Constants.VAR_TOKEN).to(Constants.TOKEN_INVALIDO);
    }

    @Entonces("^la api de pacientes contesta con codigo de error \"([^\"]*)\"$")
    public void laApiDePacientesContestaConCodigoDeError(String codigo) {
        actor.should(seeThatResponse("Codigo de error " + codigo + " entregado correctamente",
                respose -> respose.statusCode(Integer.parseInt(codigo))));
    }

    @Cuando("^se tiene la informacion incompleta de un paciente$")
    public void seTieneLaInformacionIncompletaDeUnPaciente() {
        actor.attemptsTo(PostRequestWithToken.executed(RequestPatient.getIncompletedPatient()));
    }

    @Dado("^que un \"([^\"]*)\" guardo previamente un paciente$")
    public void queUnGuardoPreviamenteUnPaciente(String name) {
        actor = Actor.named(name);
        actor.attemptsTo(CreatedPatientRequest.executed());
    }

    @Y("^el auxiliar de enfermeria conoce la ruta de la api de consulta de paciente$")
    public void elAuxiliarDeEnfermeriaConoceLaRutaDeLaApiDeConsultaDePaciente() {
        endpoint = variables.optionalProperty("apipacientescovid.getpatient")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/");
        endpoint = endpoint.concat(Serenity.sessionVariableCalled(Constants.VAR_PATIENT_ID));
        actor.whoCan(CallAnApi.at(endpoint));
    }

    @Cuando("^se consulta la informacion del paciente$")
    public void seConsultaLaInformacionDelPaciente() {
        actor.attemptsTo(GetRequestWithToken.executed());
    }

    @Entonces("^la api entrega la informacion del paciente$")
    public void laApiEntregaLaInformacionDelPaciente() {
        actor.should(seeThatResponse("Paciente encontrado correctamente",
                respose -> respose.statusCode(200)));
        patientRecieved = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonPatient.class);
        assertThat(patientRecieved).hasNoNullFieldsOrProperties();
    }

    @Dado("^que un \"([^\"]*)\" tiene un numero de documento de paciente no existente$")
    public void queUnTieneUnNumeroDeDocumentoDePacienteNoExistente(String name) {
        actor = Actor.named(name);
        actor.attemptsTo(TokenRequest.executed());
        Serenity.setSessionVariable(Constants.VAR_PATIENT_ID).to(RequestPatient.getNewPatientId());
    }

    @Y("^el auxiliar de enfermeria conoce la ruta de la api de consulta de todos los pacientes$")
    public void elAuxiliarDeEnfermeriaConoceLaRutaDeLaApiDeConsultaDeTodosLosPacientes() {
        endpoint = variables.optionalProperty("apipacientescovid.getallpatient")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/all");
        actor.whoCan(CallAnApi.at(endpoint));
    }

    @Cuando("^se consulta la informacion de todos los paciente$")
    public void seConsultaLaInformacionDeTodosLosPaciente() {
        actor.attemptsTo(GetRequestWithToken.executed());
    }

    @Entonces("^la api entrega una lista con la informacion de todos los pacientes$")
    public void laApiEntregaUnaListaConLaInformacionDeTodosLosPacientes() {
        actor.should(seeThatResponse("Respuesta exitosa de la api de consulta de todos los pacientes",
                respose -> respose.statusCode(200)));
        patients = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonPatient[].class);
        Arrays.stream(patients).forEach(x-> assertThat(x).hasNoNullFieldsOrProperties());
    }

    @Y("^el auxiliar de enfermeria conoce la ruta de la api de eliminacion de paciente$")
    public void elAuxiliarDeEnfermeriaConoceLaRutaDeLaApiDeEliminacionDePaciente() {
        endpoint = variables.optionalProperty("apipacientescovid.deletepatient")
                .orElse("http://localhost:8090/paciente-covid/api/paciente/delete/");
        endpoint = endpoint.concat(Serenity.sessionVariableCalled(Constants.VAR_PATIENT_ID));
        actor.whoCan(CallAnApi.at(endpoint));
    }

    @Cuando("^se elimina la informacion del paciente$")
    public void seEliminaLaInformacionDelPaciente() {
        actor.attemptsTo(DeleteRequestWithToken.executed());
    }

    @Entonces("^la api entrega el resultado exitoso en la eliminacion$")
    public void laApiEntregaElResultadoExitosoEnLaEliminacion() {
        actor.should(seeThatResponse("Respuesta exitosa de la api de eliminación de paciente",
                respose -> respose.statusCode(200)));
    }
}
